# Changelog

## Unreleased

### Fixed

- !4796 Correction pour SPIP 5
- #4789 Compatible SPIP 4.*
- #4792 Utililser la bonne chaîne de langue pour "Fermer" dans la charte des alertes
