<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dev?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// D
	'dev_description' => 'أدوات التطوير',
	'dev_slogan' => 'أدوات التطوير',
];
