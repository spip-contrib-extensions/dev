<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dev?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// D
	'dev_description' => 'Herramientas de desarrollo',
	'dev_slogan' => 'Herramientas de desarrollo',
];
