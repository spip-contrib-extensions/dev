<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dev?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// D
	'dev_description' => 'ابزارهاي توسعه',
	'dev_slogan' => 'ابزارهاي توسعه',
];
