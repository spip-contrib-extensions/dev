<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dev?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// D
	'dev_description' => 'Strumenti di sviluppo',
	'dev_slogan' => 'Strumenti di sviluppo',
];
