<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dev?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// D
	'dev_description' => 'Ferramentas de desenvolvimento',
	'dev_slogan' => 'Ferramentas de desenvolvimento',
];
