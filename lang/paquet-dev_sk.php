<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-dev?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// D
	'dev_description' => 'Nástroje na vývoj',
	'dev_slogan' => 'Nástroje na vývoj',
];
